'use strict'

const IP = "192.168.56.118";
const port = process.env.PORT || 3100;

const URL_WS_COCHES = `http://${IP}:3000/api`; // aquí pondremos cada uno de los servicios que creemos
const URL_WS_VUELOS = `http://${IP}:3200/api`;
const URL_WS_HOTELES = `http://${IP}:3300/api`;
const URL_WS_ACCOUNT = `http://${IP}:3400/api`;
const URL_WS_PAGOS = `http://${IP}:3500/api`;
const URL_WS_TRANSACCIONES = `http://${IP}:3600/api`;

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pen'),
    cert: fs.readFileSync('./cert/cert.pem')
};


const { json } = require('express');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');

var app = express();

//Declaramos los middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./doc/API.json');

app.use('/api/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
//app.use('/api/v1', router);


// Declaramos nuestro middleware de autorización
function auth (req, res, next) {
    if (!req.headers.authorization) {
        res.status(401).json({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token"
        });
        return next(new Error("Falta token de autorización"));
    }

    
    const queToken = req.headers.authorization.split(" ")[1];

    if ( queToken === "MITOKEN123456789") {
        req.params.token = queToken; // propiedad para propagar el token
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });
    return next(new Error("Acceso no autorizado."));
}

// Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next)=> {
    // db.getCollectionNames((err, colecciones)=> {
    //     if (err) return next(err); // Llamamos al siguiente middleware propangando el error

    //     console.log(colecciones);
    //     res.json({
    //         result: "ok",
    //         colecciones: colecciones
    //     });
    // });
});

app.get('/api/:colecciones', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    var queURL = null;
    //console.log(IP);

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}`;
    }
    if (queColeccion == "user") {
        queURL = `${URL_WS_ACCOUNT}/${queColeccion}`;
    }

    fetch(queURL)
        .then(resp => resp.json())
        .then(json => {
        //lógica de negocio
        res.json( {
            resultado: json.result,
            colecciones: queColeccion,
            elementos: json.elementos
        });
    });
});

app.get('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    var queURL = null;

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "user") {
        queURL = `${URL_WS_ACCOUNT}/${queColeccion}/${queID}`;
    }

    fetch(queURL)
        .then(resp => resp.json())
        .then(json => {
        //lógica de negocio
        res.json( {
            resultado: json.result,
            colecciones: queColeccion,
            elemento: json.elemento
        });
    });
});

app.post('/api/:colecciones', auth, (req, res, next) => {
    
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;
    var queURL = null;
    const queToken = req.params.token;

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}`;
    }
    if (queColeccion == "signup") {
        queURL = `${URL_WS_ACCOUNT}/signup`;
    }
    if (queColeccion == "login") {
        queURL = `${URL_WS_ACCOUNT}/login`;
    }

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(nuevoElemento),
                    headers: {
                        'Content-type' : 'application/json',
                        'Authorization' : `Bearer ${queToken}`
                    }
                })
        .then(resp => resp.json())
        .then(json => {
        //lógica de negocio
        res.json( {
            result: "ok",
            colecciones: queColeccion,
            elementos: json.elementos
        });
    });


    // req.collection.save(nuevoElemento, (err, elementoGuardado)=> {
    //     if (err) return next(err);

    //     console.log(elementoGuardado);
    //     res.status(201).json({
    //         result: "ok",
    //         colección: queColeccion,
    //         elemento: elementoGuardado
    //     });
    // });
});

app.put('/api/:colecciones/reserva/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    var queURL = null;
    const queToken = req.params.token;

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}/reserva/${queID}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}/reserva/${queID}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}/reserva/${queID}`;
    }
    if (queColeccion == "signup") {
        queURL = `${URL_WS_ACCOUNT}/signup`;
    }
    if (queColeccion == "login") {
        queURL = `${URL_WS_ACCOUNT}/login`;
    }

    fetch(queURL, {
        method: 'PUT',
        headers: {
            'Content-type' : 'application/json',
            'Authorization' : `Bearer ${queToken}`
        }
    })
        .then(resp => resp.json())
        .then(json => {
            //lógica de negocio
            res.json( {
            result: "ok",
            colecciones: queColeccion,
            elementos: json.elementos
        });
    });

    // req.collection.update(
    //     { _id: id(queID) },
    //     (err, resultado) => {
    //         if (err) return next(err);

    //         console.log(resultado);
    //         res.json({
    //             result: "ok",
    //             coleccion: queColeccion,
    //             elemento: queID,
    //             resultado: resultado
    //         });
    //     }
    // );
});

app.put('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    const nuevosDatos = req.body;
    var queURL = null;
    const queToken = req.params.token;

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "signup") {
        queURL = `${URL_WS_ACCOUNT}/signup`;
    }
    if (queColeccion == "login") {
        queURL = `${URL_WS_ACCOUNT}/login`;
    }

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-type' : 'application/json',
            'Authorization' : `Bearer ${queToken}`
        }
    })
        .then(resp => resp.json())
        .then(json => {
            //lógica de negocio
            res.json( {
            result: "ok",
            colecciones: queColeccion,
            elementos: json.elementos
        });
    });

    // req.collection.update(
    //     { _id: id(queID) },
    //     (err, resultado) => {
    //         if (err) return next(err);

    //         console.log(resultado);
    //         res.json({
    //             result: "ok",
    //             coleccion: queColeccion,
    //             elemento: queID,
    //             resultado: resultado
    //         });
    //     }
    // );
});

app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    //const queFecha = req.query.fecha; esto es un filtro
    var queURL = null;
    const queToken = req.params.token;

    if (queColeccion == "coches") {
        queURL = `${URL_WS_COCHES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "vuelos") {
        queURL = `${URL_WS_VUELOS}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "hoteles") {
        queURL = `${URL_WS_HOTELES}/${queColeccion}/${queID}`;
    }
    if (queColeccion == "signup") {
        queURL = `${URL_WS_ACCOUNT}/signup`;
    }
    if (queColeccion == "login") {
        queURL = `${URL_WS_ACCOUNT}/login`;
    }

    fetch(queURL, {
        method: 'DELETE',
        headers: {
            'Content-type' : 'application/json',
            'Authorization' : `Bearer ${queToken}`
        }
    })
        .then(resp => resp.json())
        .then(json => {
            //lógica de negocio
            res.json( {
            result: "ok",
            colecciones: queColeccion,
            elementos: json.elementos
        });
    });


    

    // req.collection.remove(
    //     { _id: id(queID) },
    //     { $set: nuevosDatos },
    //     { safe: true, multi: false },
    //     (err, resultado) => {
    //         if (err) return next(err);

    //         console.log(resultado);
    //         res.json({
    //             result: "ok",
    //             coleccion: queColeccion,
    //             resultado: resultado
    //         });
    //     }
    // );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API GW ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});

// app.listen(port, () => {
//     console.log(`WS API GW del WS REST CRUD ejecutándose en http://localhost:${port}/api/:colecciones/:id`);
// });